import logo from './logo.svg';
import './App.css';
import React, { useContext, useState } from 'react';
import Head from './component/Layout/Head';
import Footer from './component/Layout/Footer';
import Menu from './component/Layout/Menu';
import Menuleft from './component/Layout/Menuleft';
import { useLocation } from 'react-router-dom';
import Menuacc from './component/Account/Menuacc';
import { UserContext } from './UserContext';
import Home from './Home';


function App(props) {
  //
  let params1 = useLocation();
  //lay cai duoi cua link de set hide cai menuleft cua toan trang web thanh menu cua account
 // console.log(params1)
 //
 var tong = 0;

 const [layqty,setLayQty] = useState("")
 function Qtylc(){
 var ct = localStorage.getItem("product")
 if(ct){
     ct = JSON.parse(ct)
     Object.keys(ct).map((key,value) =>{
         if(key == key){
         }
         // console.log("qty da click",ct[key])

         tong = tong + ct[key]


     })
 }
 console.log("qty",tong)
  setLayQty(tong)
 }
 function qtyCart(data){
    console.log("test lay sl sp qua app",data)
    setLayQty(data)//phai set lai rồi truyền qua thì mới được

    return(
      <p>Cart xxx</p>
    )
 }
//  const qtyCart = (data) => {
//   console.log("test lay sl sang app dung const",data)
//  }
 const user = {name: 'kha', logeedIn: true}
  return (
    <>
        <UserContext.Provider value={{
          qtyCart:qtyCart,
          layqty:layqty
          // dataqty:dataqty
        }}>
        <Head/>
          <section>
            <div className='container'>
              <div className='row'>
                {params1['pathname'].includes('account') ? <Menuacc/> : <Menuleft/>}
                {props.children}
              </div>
            </div>
          </section>
          <Footer/>
          {/* <h1>day la component App</h1>
          <Home/> */}
        </UserContext.Provider>
    </>
  );
}

export default App;
