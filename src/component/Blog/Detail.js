import axios, { Axios } from "axios";
import { useState, useEffect, useContext } from "react";
import { useParams } from "react-router-dom"
import { UserContext } from "../../UserContext";
import Comment from "./Comment";
import Listcomponent from "./Listcomponent ";
import Rate from "./Rate";


function Detail(props) {
	let params = useParams();
	const [data, setData] = useState('');
	const [comment,setComment]=useState([]);
	const [idRely,setIdRely]=useState("");
	function Getcmt(datacmt){
		let noi = comment.concat(datacmt)
		//noi comment với datacmt được lấy từ bên cmt qua để hiển thị
		setComment(noi)	
		//nối xong thì set vào set vào lại
	}
	function Getid(dataid){
		console.log('test lay id reply',dataid)
		
		setIdRely(dataid)
		//set id lấy được bên listcmt vào Setidreply
		console.log(idRely)	

	}
	useEffect(() => {
		axios.get("http://localhost/laravel/laravel/public/api/blog/detail/" + params.id)
			.then(Response => {
				setData(Response.data.data)
				setComment(Response.data.data.comment)
				// setIdRely(Response.data.data.comment)
				// console.log(Response.data.data.comment)

			})
			.catch(function (error) { console.log(error) }
			)

	}, [])
	// console.log(datacmt)

	function Datarender() {	
		return (
			<div class="blog-post-area">
			<h2 class="title text-center">Latest From our Blog</h2>
			<div className="single-blog-post">
				<h3>{data.title}</h3>
				<div className="post-meta">
					<ul>
						<li><i className="fa fa-user"></i> Mac Doe</li>
						<li><i className="fa fa-clock-o"></i> {data.created_at}</li>
						<li><i className="fa fa-calendar"></i> {data.updated_at}</li>
					</ul>
				</div>
				<img src={"http://localhost/laravel/laravel/public/upload/blog/image/" + data.image} alt="" />
				<p>{data.description}</p>
				<div className="pager-area">
					<ul className="pager pull-right">
						<li><a href="#">Pre</a></li>
						<li><a href="#">Next</a></li>
					</ul>
				</div>
			</div>
		</div>
		)
	}
	return (
		<div class="col-sm-9">
		<div className="blogdetail">
			{Datarender()}
			<Rate/>
			<Listcomponent comment ={comment} Getid= {Getid}/>
			<Comment Getcmt={Getcmt} idRely={idRely}/> 
			{/* goi function Getcmt qua bên component Comment */}
		</div>
	</div>
	)
}
export default Detail