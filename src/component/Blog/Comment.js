import axios, { Axios } from "axios";
import { useState } from "react";
import { Link, useParams } from "react-router-dom";

function Comment(props){
    const [errors,setErrors] = useState("");
    const [comment,setComment] = useState("");
    let params = useParams();
    const handleInput = (e) => {
        setComment(e.target.value)
    }
    function Postcomment(e) {
        e.preventDefault();
        const checklogin = localStorage.getItem('checklogin')
        var x = 1 
        if(checklogin){
            // console.log("da login")
        }
        else{
            x = 2
            alert("Chưa đăng nhập")
        }
        if(comment == ''){
            x = 2
            setErrors("Chưa nhập bình luận")
        }
        if(x == 1){
            setErrors('')
            alert("ok")
            const token = JSON.parse(localStorage['token'])
            const userData = JSON.parse(localStorage['auth'])
            let url = 'http://localhost/laravel/laravel/public/api/blog/comment/' + params.id
            let accessToken = token
            let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
            };
                const formData = new FormData();
                    formData.append('id_blog',params.id);
                    formData.append('id_user',userData.id);
                    formData.append('id_comment', props.idRely ? props.idRely : 0);
                    formData.append('comment', comment);
                    formData.append('image_user',userData.avatar);
                    formData.append('name_user',userData.name)

                    axios.post(url,formData,config)
                    .then(response=>{
                        console.log(response.data.data)
                        // console.log(props.idRely)
                        props.Getcmt(response.data.data)
                        //lấy data cmt truyền qua detail
                    })
        }
        
    }
    return (
        <div className="Comment">
            <div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<h2>Leave a replay</h2>
								
								<div class="text-area">
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									    <span>{errors}</span>
								    	<textarea id="cmt" value={comment} rows="11"onChange={handleInput}></textarea>
                                        <Link onClick={Postcomment} class="btn btn-primary" to="">post comment</Link>

								</div>
							</div>
						</div>
					</div>
        </div>
    )
}
export default Comment