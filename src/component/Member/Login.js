import axios from "axios";
import { useState } from "react";
import Error from "../Error/Error";
import {useNavigate} from "react-router-dom";

function Login() {
    const [inputs,setInputs] = useState({
        email:'',
        password:'',
        level:''
    });
    const [errors,setErrors] = useState({});

    const navigate = useNavigate();

    const handleInput = (e) =>{
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))
    }
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
    const handleSubmit = (e) => {
        e.preventDefault();

        var x = 1;
        let errorSubmit = {}
        let flag = true;
            
            if(inputs.email == '') {
                x = 2;
               flag = false;
                errorSubmit.email = " Chưa nhập Email"
            }
            else if( !validateEmail(inputs.email)) {
                x = 2;        
                flag = false;
                errorSubmit.email = "Đây không phải là Email"
            }
            if(inputs.password == '') {
                x = 2;
                flag = false;
                errorSubmit.password     = "Chưa nhập password";
            }
            if(inputs.level == ""){
                x = 2;
                flag = false;
                errorSubmit.level = "Chưa chon level"
            }  
          
        if(!flag) {
            x = 2;
            setErrors(errorSubmit);
        }
        if(x == 1){
            const data = {
                email:inputs.email,
                password:inputs.password,
                level:inputs.level
            }
            axios.post("http://localhost/laravel/laravel/public/api/login",data)
            .then((res)=> {
                if(res.data.errors){
                    setErrors(res.data.errors)
                }else{
                    alert("Login thanh cong")
                    navigate("/")
                    
                    var v = true;
                    localStorage.setItem('checklogin',JSON.stringify(v))
                    console.log(res)
                    // console.log(res.data.Auth)
                    //console.log(res.config.data)
                    var datauser = res
                    var auth = res.data.Auth
                    // console.log(res.data.success.token)
                    var token = res.data.success.token
                    localStorage.setItem('datauser',JSON.stringify(datauser))
                    localStorage.setItem('token',JSON.stringify(token))
                    localStorage.setItem('auth',JSON.stringify(auth))
                    
                }
            })        
    }
 }
    return(
        <form onSubmit = {handleSubmit}>
                <input type="email" placeholder="Email Address" name="email" onChange={handleInput}/>
                <input type="password" placeholder="Password" name="password" onChange={handleInput}/>

                <span>
                <input type="checkbox" name="level" onChange={handleInput} value="0" class="checkbox"/>
					Keep me signed in
				</span>
                <ul className="rendererror">
                <Error errors={errors}/></ul>
                <button type="submit" class="btn btn-default">Login</button>			
            </form>
    )
}

export default Login