import axios from "axios";
import { useState,setState } from "react"
import Error from "../Error/Error";

function Register() {
    const [inputs,setInputs] = useState({
        name:'',
        email:'',
        password:'',
        phone:'',
        address:'',
        level:''
    });
    const [file,setFile] = useState('')
    const [avatar,setAvatar] = useState('')
    const [errors,setErrors] = useState({});

    const checkimg = ["png","jpg","jpeg","PNG","JPG"] 
    const handleInput = (e) =>{
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))
    }

    function handleImgFile  (e) {

        const file = e.target.files        
        setFile(file)
        let reader = new FileReader();
        reader.onload= (e) => {
                setAvatar(e.target.result);
                setFile(file[0]);
            };
        
        reader.readAsDataURL(file[0])
    }
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        var x = 1;
        let errorSubmit = {}
        let flag = true;
        
        if(inputs.name == "") {
            x = 2;
            flag = false;
            errorSubmit.name = " Chưa nhập Name"
        }
        if(inputs.email == "") {
            x = 2;
            flag = false;
            errorSubmit.email = " Chưa nhập Email"
        }
        else if( !validateEmail(inputs.email)) {
                x = 2;
                errorSubmit.email = "Đây không phải là Email"
        }
        if(inputs.password == "") {
            x = 2;
            flag = false;
            errorSubmit.password = "Chưa nhập password";
        }
        if(inputs.phone == ""){
            x = 2;
            flag = false;
            errorSubmit.phone = "Chưa nhap phone"
        }
        if(inputs.address == ""){
            x = 2;
            flag = false;
            errorSubmit.address = "Chưa nhap address"
        }
        if(inputs.level == ""){
            x = 2;
            flag = false;
            errorSubmit.level = "Chưa chon level"
        }
        if(file == ""){
            x = 2;
            flag = false;
            errorSubmit.img = "Chưa chọn Avatar"
        }
        else{
        const getName = file.name;
        const s = getName.split(".")
        const t = s[1]
        const demo = checkimg.includes(t)
            if (file.size > 1024 * 1024){
                x = 2;
                flag = false;
                errorSubmit.img = "file quá lớn"
            } 
            else if (demo == false) {
                x = 2;
                flag = false;
                errorSubmit.img = "đây k phải là hình ảnh" 
        }
    }
        if(!flag) {
            x = 2;

            setErrors(errorSubmit);
        }
        if(x == 1){
            const data = {
                name:inputs.name,
                email:inputs.email,
                password:inputs.password,
                phone:inputs.phone,
                address:inputs.address,
                level:inputs.level,
                avatar:avatar
            }
            axios.post("http://localhost/laravel/laravel/public/api/register",data)
            .then((res)=> {
                if(res.data.errors){
                    setErrors(res.data.errors)
                }else{
                    alert("dang ki thanh cong")
                    console.log(res)
                }
            })  
        }
        
        
        
    } 

    return (
        
            <form onSubmit = {handleSubmit} className="form-register" form enctype="multipart/form-data">
                <input type="text" placeholder="Name" name="name" onChange={handleInput}/>
                <input type="text" placeholder="Email" name="email" onChange={handleInput}/>
                <input type="password" placeholder="Password" name="password" onChange={handleInput}/>
                <input type="text" placeholder="Phone" name="phone" onChange={handleInput}/>
                <input type="text" placeholder="Address" name="address" onChange={handleInput}/>
                <input type="file" name="img" onChange={handleImgFile} className="climg"/>
                <span>
                <input type="checkbox" name="level" onChange={handleInput} value="0" class="checkbox"/>Level
                </span>
                <ul className="rendererror">
                <Error errors={errors}/>   
                </ul>
                <button type="submit">Signup</button>
            </form>
		

    )

}
export default Register