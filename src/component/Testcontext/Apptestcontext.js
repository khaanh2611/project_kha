import Childen from "./Childen"
import Childen1 from "./Childen1"
import { Mycontext } from "./Mycontext"

function Apptestcontext (){
    const testnd = "day la test nd";
    const user = {name: 'kha', loggedIn: true}
    return(
        <Mycontext.Provider value={{
            testnd:testnd,
            user:user
        }}>
        <h1>Day la app</h1>
            <Childen/>
            <Childen1/>
        </Mycontext.Provider>
    )
}
export default Apptestcontext