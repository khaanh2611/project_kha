import { Link } from "react-router-dom"

function Menuacc(){
    return(<div className="col-sm-3">
    <div className="left-sidebar">
        <h2>Account</h2>
        <div className="panel-group category-products" id="accordian">
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                            <span className="badge pull-right"><i className="fa fa-plus"></i></span>
                            <Link to="/account/product/myproduct">My Product</Link>

                        </a>
                    </h4>
                </div>
                <div className="panel-heading">
                    <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                            <span className="badge pull-right"><i className="fa fa-plus"></i></span>
                            <Link to="/account/update">My Account</Link>
                            {/* MyProduct */}
                        </a>
                    </h4>
                </div>
                <div className="panel-heading">
                    <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                            <span className="badge pull-right"><i className="fa fa-plus"></i></span>
                            <Link to="/account/product/editproduct/:id">Edit Product</Link>
                        </a>
                    </h4>
                </div>

                
            </div>
        </div>
    </div>
</div>
)
}
export default Menuacc
