import axios from "axios";
import { useEffect, useState } from "react";
import Error from "../Error/Error";
import { useLocation, useParams } from "react-router-dom"


function Update(){
    let params1 = useLocation();
    // console.log(params1)
    const [file,setFile] = useState('')
    const [avatar,setAvatar] = useState('')
    const [errors,setErrors] = useState({});
    const [inputs,setInputs] = useState({
        name:  "",
        email: "",
        password: "",
        phone: "",
        address: "",
    });
    const checkimg = ["png","jpg","jpeg","PNG","JPG"] 

    useEffect(() => {
        const userData = JSON.parse(localStorage['datauser'])
            console.log('data: ', userData.data)
            const data = userData.data.Auth
            console.log(data)
           setInputs({
                name:  data?.name,
                email: data?.email ,
                password: '',
                phone: data?.phone,
                address: data?.address,
            });
   }, [])
    const handleInput = (e) =>{
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))   
    }


    function handleImgFile  (e) {
        const file = e.target.files        
        setFile(file)
        let reader = new FileReader();
        reader.onload= (e) => {
                setAvatar(e.target.result);
                setFile(file[0]);
            };
        reader.readAsDataURL(file[0])
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        var x = 1;
        let errorSubmit = {}
        let flag = true;
        
        if(inputs.name == '') {
            x = 2;
            flag = false;
            errorSubmit.name = " Tên không được để trống"
        }
        if(inputs.phone == ""){
            x = 2;
            flag = false;
            errorSubmit.phone = "Phone không được để trống"
        }
        if(inputs.address == ''){
            x = 2;
            flag = false;
            errorSubmit.address = "Địa chỉ không được để trống"
        }
        if(file == ""){
            x = 2;
            flag = false;
            errorSubmit.img = "Chưa chọn Avatar"
        }
        else{
        const getName = file.name;
        const s = getName.split(".")
        const t = s[1]
        const demo = checkimg.includes(t)
            if (file.size > 1024 * 1024){
                x = 2;
                flag = false;
                errorSubmit.img = "file quá lớn"
            } 
            else if (demo == false) {
                x = 2;
                flag = false;
                errorSubmit.img = "đây không phải là hình ảnh" 
        }
    }
        if(!flag) {
            x = 2;

            setErrors(errorSubmit);
        }
        if(x == 1){
            alert('do')
            // console.log(inputs.name)
            const token = JSON.parse(localStorage['token'])
            const userData = JSON.parse(localStorage['auth'])
			//console.log(token)
            //console.log(userData.id)
            let url = 'http://localhost/laravel/laravel/public/api/user/update/' + userData.id
			//console.log(url)
            let accessToken = token
            let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
            };
			//console.log(config)
                const formData = new FormData();
                    formData.append('name', inputs.name);
                    formData.append('email',inputs.email);
                    // formData.append('user_id',userData.id);
                    formData.append('password', inputs.password ? inputs.password : "");
                    formData.append('phone', inputs.phone);
                    formData.append('address', inputs.address);
                    formData.append('avatar', avatar);
                axios.post(url,formData,config)
                .then(response=>{
                    console.log(response)
                    // // console.log(res.data.Auth)
                    // //console.log(res.config.data)
                    var datauser = response
                    var auth = response.data.Auth
                    var token = response.data.success.token

                    // console.log(res.data.success.token)
                    localStorage.setItem('datauser',JSON.stringify(datauser))
                    localStorage.setItem('token',JSON.stringify(token))
                    localStorage.setItem('auth',JSON.stringify(auth))

                })
        }
        
        
        
    } 

    return (
        
        <div class="col-sm-8">
			<div class="signup-form">
				<h2>User Update!</h2>
                <form onSubmit = {handleSubmit} className="form-register" form enctype="multipart/form-data">
                    <input type="text" name="name" value={inputs.name} onChange={handleInput}/>
                    <input type="text" placeholder="Email" name="email" value={inputs.email} onChange={handleInput} readOnly/>
                    <input type="password" placeholder="Password" name="password" value={inputs.password}  onChange={handleInput}/>
                    <input type="text" placeholder="Phone" name="phone" value={inputs.phone} onChange={handleInput}/>
                    <input type="text" placeholder="Address" name="address" value={inputs.address} onChange={handleInput}/>
                    <input type="file" name="img" onChange={handleImgFile} className="climg"/>
                    <ul className="rendererror">
                    <Error errors={errors}/>   
                    </ul>
                    <button type="submit">Update</button>
                </form>
    	    </div>
		</div>
        
        )
}
export default Update