import axios from "axios"
import { useContext, useEffect, useState } from "react"
import context from "react-bootstrap/esm/AccordionContext"
import { Link } from "react-router-dom"
import { UserContext } from "../../UserContext"

function Homeproduct(props){
    const testcontext = useContext(UserContext)
    console.log("test call data context ",testcontext)
    const [data,setData] = useState("")
    var tong = 0;
    var testlc = localStorage.getItem("product")
        // console.log("sl",testlc)
        if(testlc){
            testlc = JSON.parse(testlc)
            Object.keys(testlc).map((key,value) =>{
                tong = tong + testlc[key]
            })
        }
        // console.log("test sl",tong)
        testcontext.qtyCart(tong)

        //
        
        // testcontext.props.qtyCart("abc")

        //
        useEffect(() => {
        axios.get("http://localhost/laravel/laravel/public/api/product")
            .then(res => {
                setData(res.data.data)
                //console.log(res.data.data)
            })
            .catch(error => console.log(error))
    }, [])
    function Addprd(e){
        // console.log("test add product",e)
        // const xx = JSON.parse(e)
        // console.log(xx)
        // var xx = e.target.id 
        // console.log("test",xx)
        // var productcha= {};
        // productcha.e=1;
        var product = {}
            // product[e] =1;
        var x = 1;
        var testlc = localStorage.getItem("product")
            //console.log("test lay",testlc)
            if(testlc){
                product = JSON.parse(testlc)
                Object.keys(product).map((key,value) =>{
                    if(key == e){
                        x = 0;
                        product[e] +=1;
                    }
                })
            }
        if(x == 1){
            product[e] = 1;
        }

        localStorage.setItem("product",JSON.stringify(product))
        // console.log("test add",product)
        tong = tong + 1
        // console.log("sl sp",tong)
        testcontext.qtyCart(tong)

    }
    function Addwishlist(e){
        var prdwl = [];
        var x = 1;
        var test = localStorage.getItem('prdwl')
            if(test){
                prdwl = JSON.parse(test)
                Object.keys(prdwl).map((key,value) =>{
                    if(prdwl == e){
                        x = 0;
                        prdwl?.push(e)
                        // delete prdwl[e]

                    }
                })
            }
            if(x == 1){
                prdwl.push(e)
            }
            localStorage.setItem('prdwl',JSON.stringify(prdwl))
            console.log(prdwl)
    }
    function Datarender(){
        if (Object.keys(data).length > 0) {
            // console.log(data)
			return Object.keys(data).map((key, index) => {
                const xx = data[key].image
                // console.log(xx)
                const myArr = JSON.parse(xx);
				return (
					<div className="col-sm-4">
                        <div className="product-image-wrapper">
                            <div className="single-products">
                                <div className="productinfo text-center" key={index}>
                                    <img src={"http://localhost/laravel/laravel/public/upload/user/product/"+data[key].id_user + "/" + myArr[0]} alt="" />
                                    <h2>${data[key].price}</h2>
                                    <p>{data[key].name}</p>
                                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                </div>
                                <div className="product-overlay">
                                    <div className="overlay-content">
                                        <h2>${data[key].price}</h2>
                                        <p>{data[key].name}</p>
                                        <a href="#" id={data[key].id} className="btn btn-default add-to-cart" onClick={() => Addprd(data[key].id)}>Add to cart</a>
                                    </div>
                                </div>
                            </div>
                            <div className="choose">
                                <ul className="nav nav-pills nav-justified">
                                    <li onClick={() =>Addwishlist(data[key].id)}><a>Add to wishlist</a></li>
                                    {/* <li><a href=""><i className="fa fa-plus-square"></i>Add to compare</a></li> */}
                                    <li>
                                    <Link to={"/product/detail/" + (data[key].id)}>                                    
                                    Read more
                                    </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
				)
			})
		}
        
    }
    return(
        <div className="col-sm-9 padding-right">
        <div className="features_items">
            <h2 className="title text-center">Features Items</h2>
                {Datarender()}
            <ul class="pagination">
                <li class="active"><a href="">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">&raquo;</a></li>
            </ul>
        </div>
    </div>
)
}
export default Homeproduct