import { Link } from "react-router-dom";
import Menu from "./Menu";
import {useNavigate} from "react-router-dom";
import { useContext } from "react";
import { UserContext } from "../../UserContext";



function Head() {
	const navigate = useNavigate();
	const testcontext = useContext(UserContext)
	console.log("test qty tren cart",testcontext)
	function Renderlogin(){
		
		const x = localStorage.getItem('checklogin')
		// const v = localStorage.getItem('token')
		// const z = localStorage.getItem('auth')
		
		if(x){
			return(
			<ul class="nav navbar-nav">
				{/* <li><a href=""><i class="fa fa-user"></i> Account</a></li> */}
				<li><Link to="account/update"><i class="fa fa-user"></i>Account</Link></li>
				{/* <li><a href=""><i class="fa fa-star"></i> Wishlist</a></li> */}
				<li><Link to="product/wishlist"><i class="fa fa-star"></i>Wishlist</Link></li>

				{/* <li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li> */}
				{/* <li><a href="cart.html"><i class="fa fa-shopping-cart" id="cartnumber"></i>Cart</a></li> */}
				<li><Link to="mycart"><i class="fa fa-shopping-cart"></i>Cart {testcontext?.layqty}</Link></li>

				{/* {Renderlogin()} */}
				<li onClick={Logout}><Link to=''>Logout</Link></li>
				</ul>
			)

		}
		else{
			return(
			<ul class="nav navbar-nav">
				<li><a href=""><i class="fa fa-star"></i> Wishlist</a></li>
				{/* <li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li> */}
				<li><a href="cart.html"><i class="fa fa-shopping-cart" id="cartnumber"></i>Cart</a></li>
				<li><Link to='member/index'>Login</Link></li>
				</ul>
			) 
		}
	}
	function Logout(){
		navigate('member/index')
		localStorage.removeItem('checklogin')
		localStorage.removeItem('token')
		localStorage.removeItem('auth')
		localStorage.removeItem('datauser')
		alert("da logout")
		// console.log("logout")
	}
	


  return (
    <div className="Head">
     
     <header id="header">
		<div class="header_top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-md-4 clearfix">
						<div class="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div>
						<div class="btn-group pull-right clearfix">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canada</a></li>
									<li><a href="">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canadian Dollar</a></li>
									<li><a href="">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-8 clearfix">
						<div class="shop-menu clearfix pull-right">
							{/* <ul class="nav navbar-nav">
								<li><Link to="account/update"><i class="fa fa-user"></i>Account</Link></li>
								<li><a href=""><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="cart.html"><i class="fa fa-shopping-cart" id="cartnumber"></i>Cart</a></li>
							</ul> */}
							{Renderlogin()}

						</div>
					</div>
				</div>
			</div>
		</div>
		<Menu/>
	</header>
    </div>
  );
}

export default Head;
