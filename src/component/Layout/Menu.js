import {
    BrowserRouter as Router,
    Route,
    Link
  } from 'react-router-dom'
function Menu(props){
    return(
        <div class="header-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-9">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <div class="mainmenu pull-left">
                <ul class="nav navbar-nav collapse navbar-collapse">
                  <li><Link to='/'  class="active">Home</Link></li>
                  

                  <li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                      <ul role="menu" class="sub-menu">
                                        <Link to="allproduct">
                                          <li><a href="shop.html">Products</a></li>
                                        </Link>
                      <li><a href="product-details.html">Product Details</a></li> 
                      <li><a href="checkout.html">Checkout</a></li> 
                      
                      <li><a href="login.html">Login</a></li> 
                                      </ul>
                                  </li> 
                  <li class="dropdown">
                <Link to='blog/indexs'>Blog<i class="fa fa-angle-down"></i></Link>
                                      <ul role="menu" class="sub-menu">
                                          <li><a href="blog.html">Blog List</a></li>
                      <li><a href="blog-single.html">Blog Single</a></li>
                                      </ul>
                                  </li> 
                                  <li><Link to=''>Member</Link></li>
                  <li><a href="contact-us.html">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="search_box pull-right">
                <input type="text" placeholder="Search"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
}
export default Menu
