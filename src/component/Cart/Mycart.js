import axios from "axios"
import { useContext, useEffect, useState } from "react"
import { UserContext } from "../../UserContext";

function Mycart(){
	const setqty = useContext(UserContext)
    console.log(setqty)
	const [data,setData] = useState({});
	var tong = 0;
	var sum = 0;
	function Datarender(){
		const xxx = localStorage.getItem("product")
		useEffect(() => {
			// const CallApi = () => {
				axios.post("http://localhost/laravel/laravel/public/api/product/cart",xxx)
				.then((res)=> {
					if(res.data.errors){
						// setData(res.data.errors)
						console.log(res.data.errors)
					}else{
						//alert("lay thanh thanh cong")
						console.log(res.data.data)
						setData(res.data.data)
					}
				
				},)
				// }
			},[])
			
		if(Object.keys(data).length > 0) {
			return Object.keys(data).map((key,index) => {
				// console.log(data)
				 const xx = JSON.parse(data[key].image)
					//console.log(xx[0])
					var ximg= xx[0]
					// console.log(ximg)
					var totalprd = (data[key].price) * (data[key].qty)
					console.log(totalprd)
					tong = tong + totalprd
					console.log("total",tong)
					sum = sum + data[key].qty
					console.log('tong qty',sum)
					setqty.qtyCart(sum)

				return(
					<tbody>
						<tr key={index}>
							<td className="cart_product">
								<a href=""><img className="img_cart" src={"http://localhost/laravel/laravel/public/upload/user/product/" + data[key].id_user +'/' + ximg	} alt="" /></a>
								
							</td>
							<td className="cart_description">
								<h4><a href="">{data[key].name}</a></h4>
								<p>Web ID: {data[key].id}</p>
							</td>
							<td className="cart_price">
								<p>${data[key].price}</p>
							</td>
							<td className="cart_quantity">
								<div className="cart_quantity_button">
								{/* <a className="cart_quantity_up" href="" id={data[key].id} onClick={() => Upqty(data[key].id)}> + </a> */}
								{/* <a className="cart_quantity_up" href="" onClick={() => handleShow(imgShow)} + </a> */}
								<a onClick={() => Upqty(data[key])}>+</a>
									<input className="cart_quantity_input" type="text" name="quantity" value={data[key].qty} autocomplete="off" size="2"/>
									{/* <a className="cart_quantity_down" href=""> - </a> */}
									<a onClick={() => Downqty(data[key])}>-</a>
								</div>
							</td>
							<td className="cart_total">
								<p className="cart_total_price">${totalprd}</p>
							</td>
							<td className="cart_delete">
								<button onClick={() => Deleteprd(data[key].id)}>Delete</button>
								{/* <a className="cart_quantity_delete" href=""><i className="fa fa-times"></i></a> */}
							</td>
						</tr>
					</tbody>


				)
			})
		}
	}
	//funtion xoa prd
	function Deleteprd(e){
		console.log("id can xoa",e)
		//xu li xoa tren giao dien
		var xxx = [...data]
			//console.log("all data khi chua xoa",xxx)
		Object.keys(xxx).map((key,value) =>{
			if(xxx[key].id == e.toString()){
				delete xxx[key]
				console.log("all data khi da xoa",xxx)

			}
		})
		//  jquery remove empty in array (xóa emtry trong 1 mảng)
		var filtered = xxx.filter(function (el) {
			return el != null;
		  });//key :jquery remove empty in array
		  console.log("all data khi đã xóa emtry",filtered);
		  //
		setData(filtered)//set data lại cho giao diện
		//
		//xu li xoa tren local
		var ct = localStorage.getItem("product")
		console.log("chua xoa",ct)
		if(ct){
			ct = JSON.parse(ct)
		}
		Object.keys(ct).map((key,value) => {
			if(key == e){
				delete ct[key]
				console.log("test del local",ct)
			}
		})
		localStorage.setItem("product",JSON.stringify(ct))
		console.log('local sau khi del',ct)
	}
	//funtion + qty cua prd
	function Upqty(e){
		console.log("lay id",e.id)
		//xử lý + qty trên giao diện
		var xxx = [...data]// coppy ra 1 biến mới
		console.log(xxx)
		Object.keys(xxx).map((key,value) =>{
			// console.log("lay cart",ct)
			if(xxx[key].id == e.id){
				xxx[key].qty +=1;
				// console.log("qty da click",xxx[key].qty)
				//  console.log("all data khi da click tren gd",xxx)
			}
		})
		setData(xxx)//set trên giao diện

			//xử lý tang qty trong local
			var ct = localStorage.getItem("product")
            if(ct){
                ct = JSON.parse(ct)
                Object.keys(ct).map((key,value) =>{
					// console.log("lay cart",ct)
                    if(key == e.id){
                        ct[key] +=1;
						// console.log("qty da click",ct)
                    }
                })
            }	
			localStorage.setItem("product",JSON.stringify(ct))
			// console.log("show",ct)	
	}
	// function - qty cua prd
	function Downqty(e){
		console.log("id",e.id)
		//xử lý - qty trên giao diện
		var xxx = [...data]// coppy ra 1 biến mới
		// console.log("data trước khi "xxx)
		Object.keys(xxx).map((key,value) =>{
			// console.log("lay cart",ct)
			if(xxx[key].id == e.id){
				xxx[key].qty -=1;
				// console.log("qty da click",xxx[key].qty)
				//  console.log("all data khi da click tren gd",xxx)
			}
			if(xxx[key].qty < 1){//nếu qty <1 thì thực hiện xóa lun món đó khỏi cart
				delete xxx[key]
			}
			
			  //
		})
			var filtered = xxx.filter(function (el) {
			return el != null;
		  });//key :jquery remove empty in array
		  console.log("all data khi đã xóa emtry",filtered);

		setData(filtered)//set data lại cho giao diện


			//xu li - qty tren giao dien
			var ct = localStorage.getItem("product")
            if(ct){
                ct = JSON.parse(ct)
                Object.keys(ct).map((key,value) =>{
					// console.log("lay cart",ct)
                    if(key == e.id){
                        ct[key] -=1;
						// console.log("qty da click",ct)
                    }
					if(ct[key] < 1){//nếu < 1 thì xóa lun
						delete ct[key]
					}
                })
            }	
			localStorage.setItem("product",JSON.stringify(ct))
			console.log("show",ct)	
	}
    return(
	<div className="col-sm-9 padding-right">
		<div className="table-responsive cart_info">
				<table className="table table-condensed">
					<thead>
						<tr className="cart_menu">
							<td className="image">Item</td>
							<td className="description"></td>
							<td className="price">Price</td>
							<td className="quantity">Quantity</td>
							<td className="total">Total</td>
							<td></td>
						</tr>
					</thead>
					{Datarender()}
				</table>
				<p className="all_total">All total: $ {tong}</p>

					</div>
				</div>

    )
}
export default Mycart