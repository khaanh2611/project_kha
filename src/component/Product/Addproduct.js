import axios from "axios";
import { useEffect, useState } from "react";
import Error from "../Error/Error";

function Addproduct(){
    // console.log(params1)
    const [getFile,setFile] = useState('');
    // const [avatar,setAvatar] = useState('');
    const [errors,setErrors] = useState({});
    const [getcategory,setCategory] = useState({});
    const [getbrand,setBrand] = useState({});
    const [inputs,setInputs] = useState({
        name:  "",
        price: "",
        category: "",
        brand: "",
        sale: "",
        company:"",
        sale:"",
        status:""

    });
    const [details,setDetails] = useState('')
    const checkimg = ["png","jpg","jpeg","PNG","JPG"] ;

    function Checksale(){
        if(inputs.status == ''){
            return(
                console.log('k sale')
            )

        }
        else if(inputs.status == 1){
            return(
                <input id="sale" type="text" placeholder="%" name="sale" value={inputs.sale} onChange={handleInput} style={{width : '200px'}}/>
            )
            }
        }

    useEffect(() => {
		axios.get("http://localhost/laravel/laravel/public/api/category-brand")
			.then(Response => {
                 //console.log(Response.data.category)
                 //console.log(Response.data.brand)
                setCategory (Response.data.category)
                setBrand (Response.data.brand)
                //console.log(Response.data.brand)
                //console.log(getbrand[0].brand)
			})
			.catch(function (error) { console.log(error) }
			)

	}, [])

 
    function categorylist() {
        if(Object.keys(getcategory).length > 0){
            return Object.keys(getcategory).map((key,index)=>{
                return (
                    <option key={index} value={getcategory[key].id}>{getcategory[key].category}</option>
                    )
            })
        }
    }
    function brandlist() {
        if(Object.keys(getbrand).length > 0){
            return Object.keys(getbrand).map((key,index)=>{
                return (
                    <option key={index} value={getbrand[key].id}>{getbrand[key].brand}</option>
                    )
            })
        }
    }
    const handleInput = (e) =>{
        const nameInput = e.target.name;
        const value = e.target.value;
        setInputs(state => ({...state,[nameInput]:value}))   
        // setInputs(e.target.value)
    }
    const handleTextarea = (e) =>{
        // const nameInput = e.target.name;
        // const value = e.target.value;
        // setInputs(state => ({...state,[nameInput]:value}))   
        setDetails(e.target.value)
    }

    function handleImgFile  (e) {
        const getFile = e.target.files       
        // avatar = e.target.files       
        //console.log(getFile)
        setFile(getFile)
        
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        var x = 1;
        let errorSubmit = {}
        let flag = true;
        
        if(inputs.name == '') {
            x = 2;
            flag = false;
            errorSubmit.name = " Tên không được để trống"
        }
        if(inputs.price == ''){
            x = 2;
            flag = false;
            errorSubmit.price = "Price không được để trống"
        }
        if(inputs.category == ""){
            x = 2;
            flag = false;
            errorSubmit.category = "category không được để trống"
        }
        if(inputs.brand == ''){
            x = 2;
            flag = false;
            errorSubmit.brand = "brand không được để trống"
        }
        if(inputs.status == ''){
            x = 2;
            flag = false;
            errorSubmit.status = "Status không được để trống"
        }
        if(inputs.status == 1){
            // x = 2;
            // flag = false;
            // errorSubmit.status = "Sale không được để trống"
            if(inputs.sale == ''){
                x = 2;
                // flag = false;
                errorSubmit.sale = "giá sale không được để trống"
            }
        }
        
        if(inputs.company == ''){
            x = 2;
            flag = false;
            errorSubmit.company = "Company không được để trống"
        }
        // if(inputs.detail == ''){
        //     x = 2;
        //     flag = false;
        //     errorSubmit.detail = "Detail không được để trống"
        // }
        if(details == ''){
            x = 2;
            flag = false;
            errorSubmit.detail = "Detail không được để trống"
        }
        if(getFile == ""){
            x = 2;
            flag = false;
            errorSubmit.img = "Chưa chọn Hinh"
        }
        else{
            Object.keys(getFile).map((key, ) => {
                //console.log(file)
                //console.log(file[key].name)
                const v = getFile[key].name
                const sss = v.split(".")
                //console.log(sss)
                const t = sss[1]
                //console.log(t)
                const demo = checkimg.includes(t)
            if (getFile.size > 1024 * 1024){
                x = 2;
                flag = false;
                errorSubmit.img = "file quá lớn"
            } 
            else if (demo == false) {
                x = 2;
                flag = false;
                errorSubmit.img = "có file không phải là hình ảnh vui lòng kiểm tra lại" 
            }
            else if (getFile.length > 3){
                x = 2;
                flag = false;
                errorSubmit.img = "tối đa chỉ úp được 3 file" 
            }
            })
        }
        if(!flag) {
            // x = 2;
            setErrors(errorSubmit);

        }
        // if(x == 2) {
        //     setErrors(errorSubmit);
        // }
        if(x == 1){
            //console.log(file)
            const token = JSON.parse(localStorage['token'])
            let url = 'http://localhost/laravel/laravel/public/api/user/add-product'
            let accessToken = token
            let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
            };
			//console.log(config)
                const formData = new FormData();
                    formData.append('name', inputs.name);
                    formData.append('price',inputs.price);
                    formData.append('category', inputs.category);
                    formData.append('brand', inputs.brand);
                    formData.append('company', inputs.company);
                    formData.append('detail', details);
                    formData.append('status', inputs.status);
                    formData.append('sale', inputs.sale);
                    Object.keys(getFile).map((item,i) =>{
                        formData.append("file[]",getFile[item]);
                        //console.log('test file',getFile[item])
                    });
                    axios.post(url,formData,config)
                    .then(response=>{
                        if(response.data.errors){
                            setErrors(response.data.errors)
                            //console.log(response)
                        }
                        else{
                        console.log(response)
                        alert('done')
                        }
                    })
        }    
    }  
    return(
        <div class="col-sm-8">
			<div class="signup-form">
				<h2>Create Product</h2>
                <form onSubmit = {handleSubmit} className="form-register" form enctype="multipart/form-data">

                    <input type="text" name="name" placeholder="Name" value={inputs.name} onChange={handleInput}/>
                    <input type="text" placeholder="Price" name="price" value={inputs.price} onChange={handleInput}/>
                    <select placeholder="Please choose category" name="category" onChange={handleInput}>
                    <option  value="">
                    Please choose category
                    </option>
                    {categorylist()}
                    </select>
                    <select placeholder="Please choose brand" name="brand" onChange={handleInput}>
                    <option value="">
                    Please choose brand
                    </option>
                    {brandlist()}
                    </select>
                    <select placeholder="Status" name="status" value={inputs.status} onChange={handleInput}>
                    <option>
                        Chọn hạng mục sp
                    </option>
                    <option value={0} >
                        new
                    </option>
                    <option value={1}>
                        sale
                    </option>
                    </select>
                    {Checksale}
                    <input type="text" placeholder="Company profile" name="company" value={inputs.company} onChange={handleInput}/>

                    <input type="file" name="img" onChange={handleImgFile} className="climg" multiple/>

                    <textarea placeholder="detail" value={details} rows="11"onChange={handleTextarea}></textarea>

                    <ul className="rendererror">
                    <Error errors={errors}/>
                    </ul>
                    <button type="submit">Add Product</button>
                </form>
    	    </div>
		</div>
        
    )
}
export default Addproduct