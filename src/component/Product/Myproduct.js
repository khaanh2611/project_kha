import axios from "axios"
import { useEffect, useState } from "react"
import { Link, useNavigate } from "react-router-dom";
import Deleteproduct from "./Deleteproduct";
import Editproduct from "./Editproduct";
import Listproduct from "./Listproduct";

function Myproduct(props){
    const navigate = useNavigate();
    const [getData,setData] = useState ({});
        //delete product
        function Delprd(dataid){
            //console.log("lay id cua sp ",dataid)
                const token = JSON.parse(localStorage['token'])
                let url = 'http://localhost/laravel/laravel/public/api/user/delete-product/'+ dataid
                let accessToken = token
                let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
                };
                axios.get(url,config)
                .then(res => {
                   setData(res.data.data)
                    console.log('data đã del',res.data.data)
                })
                .catch(error => console.log(error))
        }
    //
    
    useEffect(() => {
            const token = JSON.parse(localStorage['token'])
            let url = 'http://localhost/laravel/laravel/public/api/user/my-product'
            let accessToken = token
            let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
            };
            axios.get(url,config)
			.then(res => {
				setData(res.data.data)

				console.log('data gốc',res.data.data)
			})
			.catch(error => console.log(error))
		}, [])  
//
//
        function Laydataedit(x){
            console.log('data truyền qua',x)
        }

//
//
    return(
    <div className="col-sm-8">
        <div className="product">
            <div className="form-product">
                {/* <div className="col-sm-8">
                    <table class="table" >
                    <thead style={{backgroundColor : '#FE980F', color : 'white'}}>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    {Datarender()}
                </table>
            </div> */}
            {/* <Listproduct getData={getData}/> */}
            <Listproduct getData={getData} Delprd={Delprd} Laydataedit={Laydataedit}/>
            </div>
            <Link to={"/account/product/addproduct"}>
            <button className="addproduct" >Add product</button>
            </Link>
            {/* <Link to={"/account/product/editproduct"}>
            <button className="addproduct" >edit product</button>
            </Link> */}
        </div>

    </div>
    )
    
}    
export default Myproduct