import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';
import Footer from './component/Layout/Footer';
import Head from './component/Layout/Head';
import Home from './Home';
import Indexs from './component/Blog/Indexs';
import Detail from './component/Blog/Detail';
import Index from './component/Member/Index';
import Update from './component/Account/Update';
import Addproduct from './component/Product/Addproduct';
import Myproduct from './component/Product/Myproduct';
import Editproduct from './component/Product/Editproduct';
import Homeproduct from './component/Allproduct/Homeproduct';
import Productdetail from './component/Allproduct/Productdetail';
import Mycart from './component/Cart/Mycart';
import Apptestcontext from './component/Testcontext/Apptestcontext';
import Wishlist from './component/Allproduct/Wishlist';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // <React.StrictMode>
    <Router>
      <App >
        <Routes>
          <Route index path='/' element={<Home />} />
          <Route path='/blog/indexs' element={<Indexs />} /> 
          <Route path='/blog/detail/:id' element={<Detail/>} /> 
          <Route path='/member/index' element={<Index />} /> 
          <Route path='/account/update' element={<Update />} /> 
          <Route path='/account/product/addproduct' element={<Addproduct />} /> 
          <Route path='/account/product/myproduct' element={<Myproduct />} /> 
          <Route path='/account/product/editproduct/:id' element={<Editproduct />} /> 
          <Route path='allproduct' element={<Homeproduct/>}/>
          <Route path='/product/detail/:id' element={<Productdetail/>}/>
          <Route path='mycart' element={<Mycart/>}/>
          <Route path='product/wishlist' element={<Wishlist/>}/>
        </Routes>
      </App>
      {/* <Apptestcontext/> */}
      
    </Router>
  // </React.StrictMode>
);
reportWebVitals();
