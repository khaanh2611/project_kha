import { useContext } from "react"
import { UserContext } from "./UserContext"

function Home (){
    const test = useContext(UserContext)
    console.log(test)
    return(
        <div className="Home">
            <h2>day la trang Home dang test context {test?.xxx}</h2>
        </div>
    )
}
export default Home